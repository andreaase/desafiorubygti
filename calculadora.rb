require_relative 'operacoes_extras'
require 'net/http'
require 'json'

module Calculadora

  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra)
      data=JSON.parse(notas)
      excluidos=lista_negra.split(" ");
      divisor=data.count

      n_sem_exc=data.select do |nt|
        not excluidos.include? nt
      end

      resultado=0

      n_sem_exc.each do |name, notatot|
        resultado +=notatot
      end
 
      divisor=n_sem_exc.size
      x= resultado.to_f
      y= divisor.to_f  

      media=x/y

      puts "\nA média é #{media}\n"

    end

    def sem_numeros(numeros)
      valores=numeros.split(" ");
      saida="";
      valores.each do |valor|
        x=valor[-2..-1] #pega os 2 últimos caracteres da string

        if (x=="00") or (x=="25") or (x=="50") or (x=="75")
          saida += "S, " #O valor é divisível por 25
        else
          saida += "N, " #O valor não é divisível por 25
        end
      end
      puts saida

    end

    def filtrar_filmes(generos, ano)
      #ordenando generos
      filmes = get_filmes #pega todos os valores do método get_filmes
      filmes = filmes[:movies]
      
      generos = generos.sort

      meus_filmes = filmes.select{
        #verifica se a interseção entre v[:genres] e generos é generos
        |f| ( f[:genres] & generos) == generos 
      }
      
      #buscar ano
      meus_filmes = meus_filmes.select{
        |f| f[:year].to_i >= ano
      }

      temp = Array.new
      meus_filmes.each do |mf|
        temp << mf[:title]
      end
      meus_filmes = temp
      return meus_filmes
    end
    

    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
    

  end

  class Menu
  
    def initialize
      obj= Calculadora::Operacoes.new
      
      puts "------------------------------"
      puts "| Bem vindo(a) à Calculadora |"
      puts "------------------------------"
      puts "\n"
      puts "Escolha a operação que deseja realizar: "
      puts "1 - Operação Média Preconceituosa"
      puts "2 - Operação Calculadora sem números"
      puts "3 - Operação Filtro de Filmes"
      puts "4 - Operação Número Primo"
      puts "0 - Sair"
      print "Opção: "

      option = gets.chomp.to_i
      puts ""    
      obj=Calculadora::Operacoes.new
      
      if option==1
        puts "Operação Média Preconceituosa"
        puts "Digite a lista de alunos dentro de chaves, com o nome do aluno entre aspas, logo depois um : e a nota "
        lista=gets.chomp
        puts "Digite a lista negra de alunos com seus nomes separados por um espaço"
        listanegra=gets.chomp
        obj.media_preconceituosa(lista,listanegra)
      
      elsif option==2
        puts "Operação Calculadora sem números"
        print "Digite os valores separados por um espaço: "
        val= gets.chomp
        obj.sem_numeros(val)
      
      elsif option==3
        puts "Operação Filtro de Filmes"
        puts ""
        puts "Digite os gêneros em inglês e separados por um espaço: "
        a = gets.chomp
        a = a.split(" ")
        #a = ["Comedy", "Drama"]
        
        
        #a=["Comedy", "Drama"]
        puts ""
        puts "Digite o ano: "
        b = gets.chomp
        b = b.to_i
        
        #b=1500
        filmes = obj.filtrar_filmes(a,b)
        
        filmes.each do |f| #pega o  retorno da função e imprime
          puts f
        end

      elsif option==4
        puts "Operação Número Primo"
        puts "Escreva o número para saber por quais números ele é divisível e para saber se o mesmo é primo: "
        n = gets.chomp.to_i
        Operacoes.new.primo(n)
      
      elsif option==0
        exit
      
      else puts "Operação Inválida"
      end


    end
  
  end

end


