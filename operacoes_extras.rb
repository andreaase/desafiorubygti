module OperacoesExtras
    def primo(entrada)
        k = 0
        puts " "
        print "O número é divisível por: "

        1.step entrada do |i|
            if entrada % i == 0
            k += 1
            print "[#{i}] "
            end
        end

        if k == 2
            puts "\nO número é primo!"
        else
            puts "\nO número não é primo!"
        end
    end
    
end
